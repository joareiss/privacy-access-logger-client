package de.pure.logger.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import de.pure.logger.Global;
import de.pure.logger.entity.XPrivacyLogEntry;

public class LogBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (context == null) {
            Global.logcatDebug("Context was null. Thus doing nothing");
            return;
        }

        XPrivacyLogEntry xPrivacyLogEntry = XPrivacyLogEntry.fromBundle(intent.getExtras());
        Global.logcatDebug("Received: " + xPrivacyLogEntry);
        if (xPrivacyLogEntry.packageName == null) {
            Global.logcatDebug("packageName was null. Thus doing nothing");
            return;
        }
        Logger.log(context, xPrivacyLogEntry);
    }
}
