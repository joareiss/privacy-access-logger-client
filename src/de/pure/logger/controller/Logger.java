package de.pure.logger.controller;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import de.pure.logger.Global;
import de.pure.logger.data.DbCom;
import de.pure.logger.entity.AppInfo;
import de.pure.logger.entity.XPrivacyLogEntry;

import java.util.ArrayList;
import java.util.TreeSet;

public class Logger {

    /**
     * Only write to DB if it's worth it. Open/Write/Close db costs time so
     * don't do this for every single LogEntry alone.
     */
    private static final int MINSIZE_WRITEDB = 1;

    /**
     * Needs to be static!
     */
    private static TreeSet<XPrivacyLogEntry> pendingLogEntries = new TreeSet<XPrivacyLogEntry>();
    private static ArrayList<AppInfo> appInfos = null;


    public static void log(Context context, XPrivacyLogEntry logEntry) {
        Global.logcatDebug("ABOUT TO LOG " + logEntry);
        if (logEntry != null && shouldEventBeLogged(context, logEntry)) {
            pendingLogEntries.add(logEntry);
            if (pendingLogEntries.size() < MINSIZE_WRITEDB) {
                return;
            }
            commitToDb(context);
        }
    }


    private static boolean shouldEventBeLogged(Context context, XPrivacyLogEntry logEntry) {
        String packageName = logEntry.packageName;
        if (packageName == null) {
            Global.logcatDebug("packageName was null");
            return false;
        }

        if (packageName.equals(Global.FAKE_LOG_PACKAGENAME)) {
            // always log fake notifications that
            // have been sent from the StartActivity
            return true;
        } else if (packageName.equals(Global.FILEWATCH_LOG_PACKAGENAME)) {
            // always log file watch notifications that
            // have been sent from the FileWatchAsyncTasks
            return true;
        } else if (packageName.equals(Global.NETWORK_LOG_PACKAGENAME)) {
            // always log notifications from network that
            // do not belong to a specified uid/package
            return true;
        } else {
            if (appInfos == null) {
                updateAppInfos(context);
            }
            for (AppInfo appInfo : appInfos) {
                if (TextUtils.equals(appInfo.packageName, packageName)) {
                    return true;
                }
            }
        }
        Global.logcatDebug("package '" + packageName + "' " +
                "not found as a package that should "
                + "be logged. Thus doing nothing");
        return false;
    }


    private static void commitToDb(final Context context) {
        try {
            final TreeSet<XPrivacyLogEntry> toWrite = new TreeSet<XPrivacyLogEntry>(
                    pendingLogEntries);
            pendingLogEntries.clear();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    Global.logcatDebug("----------------thread" + Thread.currentThread().getId());
                    Global.logcatDebug("----------------" + pendingLogEntries.size());
                    try {
                        boolean success = DbCom.writeXPrivacyLogEntries(DbCom.getDatabase(context),
                                toWrite);
                        if (success) {
                            Global.logcatDebug("List of logs written to database successfully. Amount: "
                                    + toWrite.size());
                        } else {
                            Global.logcatError("List of logs written to database NOT successfully.");
                        }
                    } catch (SQLiteException e) {
                        Global.logcatError("error: " + e.getMessage());
                        Global.logcatError("----------------" + pendingLogEntries.size());
                        Global.logcatError("----------------thread" + Thread.currentThread().getId());
                    }
                }
            }).start();
        } catch (Exception e) {
            Global.logcatError(e.getMessage());
        }
    }

    public static void updateAppInfos(Context context) {
        appInfos = DbCom.getAllAppInfos(DbCom.getDatabase(context));
    }

}
