package de.pure.logger.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import de.pure.logger.R;
import de.pure.logger.entity.AppInfoListItem;

public class AppInfoAdapter extends ArrayAdapter<AppInfoListItem> {
    private final Context context;

    public AppInfoListItem[] values;

    public AppInfoAdapter(Context context, AppInfoListItem[] values) {
        super(context, R.layout.whattologlistitem, values);
        this.context = context;
        this.values = values;
    }

    static class ViewHolder {
        protected TextView tv_appname;
        protected CheckBox cb_check;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        ViewHolder viewHolder = null;
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inf.inflate(R.layout.whattologlistitem, null);
            viewHolder = new ViewHolder();
            viewHolder.tv_appname = (TextView) convertView.findViewById(R.id.whattologlistitem_tv_appname);
            viewHolder.cb_check = (CheckBox) convertView.findViewById(R.id.whattologlistitem_cb_check);
            viewHolder.cb_check.setOnCheckedChangeListener(
                    new CompoundButton.OnCheckedChangeListener() {

                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            int getPosition = (Integer) buttonView.getTag();
                            // Here we get the position that we have set for the checkbox using setTag.

                            values[getPosition].checked = buttonView.isChecked();
                            // Set the value of checkbox to maintain its state.
                        }
                    });
            convertView.setTag(viewHolder);
            convertView.setTag(R.id.whattologlistitem_tv_appname, viewHolder.tv_appname);
            convertView.setTag(R.id.whattologlistitem_cb_check, viewHolder.cb_check);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.cb_check.setTag(position); // This line is important.

        viewHolder.tv_appname.setText(values[position].appName);
        viewHolder.cb_check.setChecked(values[position].checked);

        return convertView;


//        LayoutInflater inf = (LayoutInflater) context
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        // use convertView to avoid OutOfMemory
//        // http://stackoverflow.com/a/11388381
//        View row = convertView;
//        if (row == null) {
//            row = inf.inflate(R.layout.whattologlistitem, parent, false);
//        }
//
//        final int pos = position;
//
//        final AppInfoListItem item = tasks[pos];
//
//        row.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(context, "Item: "
//                        + item.toString(), Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        TextView textView = (TextView) row.findViewById(R.id.label);
//        final CheckBox checkBox = (CheckBox) row.findViewById(R.id.check);
//
//        if (item.checked) {
//            checkBox.setChecked(true);
//        } else {
//            checkBox.setChecked(false);
//        }
//
//        checkBox.setOnCheckedChangeListener(
//                new CompoundButton.OnCheckedChangeListener() {
//                    @Override
//                    public void onCheckedChanged(
//                            CompoundButton compoundButton, boolean b) {
//                        item.checked = b;
//                        tasks[pos] = item;
//                        checkBox.setChecked(b);
//                    }
//                }
//        );
//
//        textView.setText(item.appName);
//
//
//        return row;
    }

}