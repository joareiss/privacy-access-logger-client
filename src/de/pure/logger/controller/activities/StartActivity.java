package de.pure.logger.controller.activities;

import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import de.pure.logger.Global;
import de.pure.logger.R;
import de.pure.logger.controller.LogBroadcastReceiver;
import de.pure.logger.data.DbCom;
import de.pure.logger.network.IPTables;
import de.pure.logger.network.InstallUtils;
import de.pure.logger.network.NFLogService;
import de.pure.logger.util.Utils;

public class StartActivity extends BaseActivity {

    public static final String PREFERENCE_FILE = "preference_file";


    private boolean isBroadCastRecvRunning;

    private boolean isNetworkTrafficLogged;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start);

        loadStateFromPrefs();

        initButtons();

        // to get the db table created..
        DbCom.getInstance(mActivity).openDatabase();


        changeStateNetworkLogging(false, false);
        changeStateBroadcastReceiver(false, false);

    }

    @Override
    protected void onStop() {
        super.onStop();
        storeStateToPrefs();
    }

    private void loadStateFromPrefs() {
        // Restore preferences
        SharedPreferences settings = getSharedPreferences(PREFERENCE_FILE, MODE_PRIVATE);
        isBroadCastRecvRunning = settings.getBoolean("isBroadCastRecvRunning", false);
        isNetworkTrafficLogged = settings.getBoolean("isNetworkTrafficLogged", false);
    }

    private void storeStateToPrefs() {
        SharedPreferences settings = getSharedPreferences(PREFERENCE_FILE, MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("isBroadCastRecvRunning", isBroadCastRecvRunning);
        editor.putBoolean("isNetworkTrafficLogged", isNetworkTrafficLogged);
        editor.commit();
    }


    @Override
    protected void onDestroy() {
        changeStateNetworkLogging(false, false);
        changeStateBroadcastReceiver(false, false);

        DbCom.getInstance(mActivity).closeDatabase();
        super.onDestroy();
    }

    private void initButtons() {

        final Button start_btn_about = (Button) findViewById(R.id.start_btn_about);
        start_btn_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAppInfo();
            }
        });


        final Button start_btn_togglereceive = (Button) findViewById(R.id.start_btn_togglereceive);
        start_btn_togglereceive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean newValue = !isBroadCastRecvRunning;
                changeStateBroadcastReceiver(newValue, true);
            }
        });

        final Button start_btn_togglenetwork = (Button) findViewById(R.id.start_btn_togglenetwork);
        start_btn_togglenetwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean newValue = !isNetworkTrafficLogged;
                changeStateNetworkLogging(newValue, true);
            }
        });

        final Button start_btn_togglefilewatch = (Button) findViewById(R.id.start_btn_togglefilewatch);
        start_btn_togglefilewatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), FileWatchActivity.class));
            }
        });

        final Button start_btn_whattolog = (Button) findViewById(R.id.start_btn_whattolog);
        start_btn_whattolog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), WhatToLogActivity.class));
            }
        });

        final Button start_btn_logresults = (Button) findViewById(R.id.start_btn_logresults);
        start_btn_logresults.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), LogResultsActivity.class));
            }
        });

        final Button start_btn_sendfakeintent = (Button) findViewById(R.id.start_btn_sendfakeintent);
        start_btn_sendfakeintent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < 100; i++) {
                    Utils.sendFakeLogEvent(mActivity);
                }
            }
        });
        final Button start_btn_send = (Button) findViewById(R.id.start_btn_send);
        start_btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), SendActivity.class));

            }
        });
    }

    /**
     * Changes the state of the BroadcastReceiver for Logging.
     * <p/>
     * Also takes care of the UI
     * Also takes care of the persistent storage of the new state
     *
     * @param newState
     * @param showResultToast
     */
    private void changeStateBroadcastReceiver(boolean newState, boolean showResultToast) {
        toggleBroadcastReceiver(newState);
        setBroadCastRecvRunning(newState);
        updateBroadcastButtonText();
        if (showResultToast) {
            showBroadcastStateToast();
        }
    }

    /**
     * Changes the state of the NetworkLogger
     * <p/>
     * Also takes care of the UI
     * Also takes care of the persistent storage of the new state
     *
     * @param newState
     * @param showResultToast
     */
    private void changeStateNetworkLogging(boolean newState, boolean showResultToast) {
        toggleNetworkLogging(newState);
        setNetworkTrafficLogged(newState);
        updateNetworkButtonText();
        if (showResultToast) {
            showNetworkStateToast();
        }
    }


    /**
     * Don't call this directly. Rather call changeStateBroadcastReceiver(true/false)
     *
     * @param enabled
     */
    private void toggleBroadcastReceiver(boolean enabled) {
        Global.logcatDebug("toggleBroadcastReceiver " + enabled);
        int state = enabled ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED
                : PackageManager.COMPONENT_ENABLED_STATE_DISABLED;
        ComponentName receiver = new ComponentName(this, LogBroadcastReceiver.class);
        getPackageManager().setComponentEnabledSetting(receiver, state,
                PackageManager.DONT_KILL_APP);
    }

    /**
     * Don't call this directly. Rather call changeStateNetworkLogging(true/false)
     *
     * @param enabled
     */
    private void toggleNetworkLogging(boolean enabled) {
        if (enabled) {
            InstallUtils.installBinaries(this);
            IPTables.deleteAndAddRules(this);
            startService(new Intent(this, NFLogService.class));
        } else {
            stopService(new Intent(this, NFLogService.class));
            IPTables.deleteRules(this);
        }
    }


    private void updateBroadcastButtonText() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Button start_btn_togglereceive = (Button) findViewById(R.id.start_btn_togglereceive);
                String button;
                if (isBroadCastRecvRunning) {
                    button = getString(R.string.start_btn_togglereceivestop);
                } else {
                    button = getString(R.string.start_btn_togglereceivestart);
                }
                start_btn_togglereceive.setText(button);
            }
        });
    }

    private void showBroadcastStateToast() {
        String toast;
        if (isBroadCastRecvRunning) {
            toast = getString(R.string.start_toast_startedRecv);
        } else {
            toast = getString(R.string.start_toast_stoppedRecv);
        }
        Toast.makeText(mActivity, toast, Toast.LENGTH_SHORT).show();
    }


    private void updateNetworkButtonText() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Button btn_start_togglenetwork = (Button) findViewById(R.id.start_btn_togglenetwork);
                String button;
                if (isNetworkTrafficLogged) {
                    button = getString(R.string.start_btn_togglenetworkstop);
                } else {
                    button = getString(R.string.start_btn_togglenetworkstart);
                }
                btn_start_togglenetwork.setText(button);
            }
        });
    }


    private void showNetworkStateToast() {
        String toast;
        if (isNetworkTrafficLogged) {
            toast = getString(R.string.start_toast_startedNetw);
        } else {
            toast = getString(R.string.start_toast_stoppedNetw);
        }
        Toast.makeText(mActivity, toast, Toast.LENGTH_SHORT).show();
    }


    private void setBroadCastRecvRunning(boolean broadCastRecvRunning) {
        isBroadCastRecvRunning = broadCastRecvRunning;
        storeStateToPrefs();
    }

    private void setNetworkTrafficLogged(boolean networkTrafficLogged) {
        isNetworkTrafficLogged = networkTrafficLogged;
        storeStateToPrefs();
    }


}
