package de.pure.logger.controller.activities;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import de.pure.logger.R;


abstract class BaseActivity extends Activity {


    protected String[] menu_options;

    protected Activity mActivity;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mActivity = this;
        this.menu_options = getResources().getStringArray(R.array.menu_options);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem info = menu.add(menu_options[0]);
        info.setIcon(android.R.drawable.ic_menu_info_details);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        if (item.getTitle().equals(menu_options[0])) {
            showAppInfo();
        }
        return super.onMenuItemSelected(featureId, item);
    }


    protected void showAppInfo() {

        final Dialog infodialog = new Dialog(mActivity);
        infodialog.setContentView(R.layout.infodialog);

        final String dlg_info_title = mActivity.getString(R.string.base_dlg_infotitle);
        infodialog.setTitle(dlg_info_title);

        TextView infodlg_tv_infotext = (TextView) infodialog.findViewById(R.id.infodlg_tv_infotext);
        infodlg_tv_infotext.setText(R.string.aboutapp);

        Button infodialog_btn_ok = (Button) infodialog.findViewById(R.id.infodialog_btn_ok);
        infodialog_btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infodialog.dismiss();
            }
        });

        infodialog.setCancelable(true);
        infodialog.show();
    }

}
