package de.pure.logger.controller.activities;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import au.com.bytecode.opencsv.CSVWriter;
import de.pure.logger.Global;
import de.pure.logger.R;
import de.pure.logger.data.DbCom;
import de.pure.logger.entity.XPrivacyLogEntry;

import java.io.*;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


public class SendActivity extends BaseActivity {

    public static final String ERRORCOLOR = "red";

    private TextView send_tv_report;

    private boolean onlySendOnce;
    private boolean onlySendLatestFile;

    private RadioButton send_rb_sendeverytensec;
    private RadioButton send_rb_sendonce;
    private RadioButton send_rb_sendlatestdbentries;
    private RadioButton send_rb_sendlatestcsvfile;

    private EditText send_ed_serveraddress;
    private EditText send_ed_serverport;

    private Button send_btn_togglesend;

    private ScrollView send_sv_report;

    // Stuff for repeating every 10 seconds


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send);
        mActivity = this;


        initButtons();
        initRadioButtons();
        initBooleansFromRadioButtons();

        send_tv_report = (TextView) findViewById(R.id.send_tv_report);
        send_tv_report.setText("");

        send_ed_serveraddress = (EditText) findViewById(R.id.send_ed_serveraddress);
        send_ed_serverport = (EditText) findViewById(R.id.send_ed_serverport);

        send_sv_report = (ScrollView) findViewById(R.id.send_sv_report);

        if (Global.report == null) {
            Global.report = new StringBuffer();
        }

        setUiAccordingToRunningState();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        setUiAccordingToRunningState();
    }


    private void initRadioButtons() {

        send_rb_sendlatestcsvfile = (RadioButton)
                findViewById(R.id.send_rb_sendlatestcsvfile);
        send_rb_sendlatestcsvfile.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        onlySendLatestFile = b;
                    }
                });

        send_rb_sendlatestdbentries = (RadioButton)
                findViewById(R.id.send_rb_sendlatestdbentries);
        send_rb_sendlatestdbentries.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        onlySendLatestFile = !b;
                    }
                });

        send_rb_sendonce = (RadioButton)
                findViewById(R.id.send_rb_sendonce);
        send_rb_sendonce.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        onlySendOnce = b;
                    }
                });

        send_rb_sendeverytensec = (RadioButton)
                findViewById(R.id.send_rb_sendeverytensec);
        send_rb_sendeverytensec.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        onlySendOnce = !b;
                    }
                });
    }

    private void initBooleansFromRadioButtons() {
        if (send_rb_sendeverytensec != null && send_rb_sendeverytensec.isChecked()) {
            onlySendOnce = false;
        } else {
            onlySendOnce = true;
        }
        if (send_rb_sendlatestdbentries != null && send_rb_sendlatestdbentries.isChecked()) {
            onlySendLatestFile = false;
        } else {
            onlySendLatestFile = true;
        }
    }

    private void initButtons() {

        send_btn_togglesend = (Button) findViewById(R.id.send_btn_togglesend);
        send_btn_togglesend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();

                // user wants to stop repeating sending
                if (Global.isSendingBeingRepeatedRightNow) {
                    Global.repeater.cancel();
                    Global.repeater.purge();
                    Global.isSendingBeingRepeatedRightNow = false;

                    // user shouldn't do anything until cancel is done
                    send_btn_togglesend.setEnabled(false);
                    send_btn_togglesend.setText(R.string.send_btn_togglesendstopping);

                    // wait with updating the ui after cancel
                    // INTERVAL_TIME should be enough time for the
                    // last started AsyncTask to be done
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            send_btn_togglesend.setEnabled(true);
                            setUiAccordingToRunningState();
                        }
                    }, Global.INTERVAL_TIME);
                    return;
                }

                // user wants to start some sending
                if (onlySendOnce) {
                    // send once
                    new SendOnceAsyncTask(mActivity, onlySendLatestFile).execute();
                } else {
                    try {
                        // send every INTERVAL_TIME
                        Global.repeater = new Timer("MyTimer", true);
                        Global.repeater.scheduleAtFixedRate(
                                new MyTimerTask(), 0, Global.INTERVAL_TIME);
                        Global.isSendingBeingRepeatedRightNow = true;
                        setUiAccordingToRunningState();
                    } catch (IllegalThreadStateException e) {
                        e.printStackTrace();
                        Global.logcatError(e.getMessage());
                        updateStatusTextView(e.getMessage(), ERRORCOLOR);

                        // change back to start if thread did not start
                        Global.isSendingBeingRepeatedRightNow = false;
                        setUiAccordingToRunningState();
                    }
                }
            }
        });

    }


    private class MyTimerTask extends TimerTask {
        public void run() {

            // execute new Task
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new SendOnceAsyncTask(mActivity, onlySendLatestFile).execute();
                }
            });

            // check after executing the Task whether it should be done again later
            try {
                Thread.sleep(1);
                // throws an InterruptedException that is necessary
                // to actually notice that the Thread got interrupted
                // http://docs.oracle.com/javase/tutorial/essential/concurrency/interrupt.html

            } catch (InterruptedException e) {
                Global.logcatDebug("Got interrupted");
                updateStatusTextView("Stopping sending every 10 seconds...");
            }
        }
    }


    private void setUiAccordingToRunningState() {
        if (Global.isSendingBeingRepeatedRightNow) {
            send_btn_togglesend.setText(R.string.send_btn_togglesendstop);
            send_rb_sendeverytensec.setEnabled(false);
            send_rb_sendonce.setEnabled(false);
            send_rb_sendlatestdbentries.setEnabled(false);
            send_rb_sendlatestcsvfile.setEnabled(false);
            send_ed_serveraddress.setEnabled(false);
            send_ed_serverport.setEnabled(false);
        } else {
            send_btn_togglesend.setText(R.string.send_btn_togglesendstart);
            send_rb_sendeverytensec.setEnabled(true);
            send_rb_sendonce.setEnabled(true);
            send_rb_sendlatestdbentries.setEnabled(true);
            send_rb_sendlatestcsvfile.setEnabled(true);
            send_ed_serveraddress.setEnabled(true);
            send_ed_serverport.setEnabled(true);
        }
        updateStatusTextView("");
    }


    private class SendOnceAsyncTask extends AsyncTask<Void, Void, Void> {


        private boolean onlySendLatestCsvFile;

        public SendOnceAsyncTask(Context context,
                                 boolean onlySendLatestCsvFile) {
            this.onlySendLatestCsvFile = onlySendLatestCsvFile;

        }


        @Override
        protected Void doInBackground(Void... params) {

            String pathAndFileName = Environment.getExternalStorageDirectory()
                    + "/" + Global.EXPORT_CSVFILE;


            String serverAddress = send_ed_serveraddress.getText().toString();
            String serverPort = send_ed_serverport.getText().toString();

            boolean isValidInput = true;
            if (TextUtils.isEmpty(serverAddress)) {
                updateStatusTextView("No server address was given");
                isValidInput = false;
            } else if (TextUtils.isEmpty(serverPort) ||
                    !TextUtils.isDigitsOnly(serverPort)) {
                updateStatusTextView("Invalid port");
                isValidInput = false;
            }
            if (!isValidInput) {
                return null;
            }


            if (onlySendLatestCsvFile) {
                sendCsvFileToServer(pathAndFileName,
                        serverAddress, Integer.parseInt(serverPort));
            } else {
                // also send csv file,
                // but first create a new csv file with latest db content
                if (saveLatestLogEntriesFromDbToCsvFile(pathAndFileName)) {
                    sendCsvFileToServer(pathAndFileName,
                            serverAddress, Integer.parseInt(serverPort));
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            updateStatusTextView("Trying to send logs...");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            updateStatusTextView("Done trying to send logs");
        }

    }

    /**
     * @param pathAndFileName where to save the db content
     * @return true if at least on log was written to file
     */
    private boolean saveLatestLogEntriesFromDbToCsvFile(String pathAndFileName) {

        ArrayList<XPrivacyLogEntry> allLogEntries = DbCom.getAllXPrivacyLogEntriesNotOlderThan(
                DbCom.getDatabase(mActivity), 60);

        if (allLogEntries.size() < 1) {
            updateStatusTextView("Amount of logs was 0", ERRORCOLOR);
            return false;
        }

        File file = new File(pathAndFileName);
        try {
            file.createNewFile();
            CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
            for (XPrivacyLogEntry logEntry : allLogEntries) {
                csvWrite.writeNext(logEntry.toStringArray());
            }
            csvWrite.close();
        } catch (Exception e) {
            Global.logcatError(e.getLocalizedMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }


    public boolean sendCsvFileToServer(String pathAndFileName,
                                       String serverAddress, int port) {
        try {
            File file = new File(pathAndFileName);
            Socket socket = new Socket(serverAddress, port);
            byte[] byteArray = new byte[(int) file.length()];
            FileInputStream fin = new FileInputStream(file);
            BufferedInputStream bin = new BufferedInputStream(fin);
            bin.read(byteArray, 0, byteArray.length);
            OutputStream os = socket.getOutputStream();
            updateStatusTextView("Sending Files...");
            os.write(byteArray, 0, byteArray.length);
            os.flush();
            socket.close();
            updateStatusTextView("File transfer complete");
        } catch (Exception e) {
            e.printStackTrace();
            final String msg = e.getMessage();
            updateStatusTextView(msg, ERRORCOLOR);
            return false;
        }
        return true;
    }


//    private class SendCsvAsyncTask extends AsyncTask<Void, Void, Boolean> {
//        private ProgressDialog dialog;
//
//        private String pathAndFileName;
//        private String serverAddress;
//        private int serverPort;
//
//
//        public SendCsvAsyncTask(String pathAndFileName,
//                                String serverAddress, int serverPort) {
//            dialog = new ProgressDialog(mActivity);
//            dialog.setCancelable(false);
//            this.pathAndFileName = pathAndFileName;
//            this.serverAddress = serverAddress;
//            this.serverPort = serverPort;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            this.dialog.setMessage(mActivity.getString(
//                    R.string.send_dlg_gettinglatest));
//            this.dialog.show();
//        }
//
//        @Override
//        protected Boolean doInBackground(Void... params) {
//            return sendCsvFileToServer(pathAndFileName,
//                    serverAddress, serverPort);
//        }
//
//        @Override
//        protected void onPostExecute(final Boolean success) {
//            if (this.dialog.isShowing()) {
//                this.dialog.dismiss();
//            }
//        }
//    }


    /**
     * Append to send_tv_report
     *
     * @param append
     */
    private void updateStatusTextView(String append) {
        updateStatusTextView(append, "");
    }

    /**
     * Append to send_tv_report , optionally with a different color
     *
     * @param append
     * @param color  hex color or whatever HTML can interpret
     */
    private void updateStatusTextView(final String append, final String color) {
        runOnUiThread(new Runnable() { // in case it's from some Thread
            public void run() {
                if (send_tv_report == null) {
                    return;
                }
                if (TextUtils.isEmpty(append)) {
                    send_tv_report.setText(Html.fromHtml(Global.report.toString()));
                    return;
                }

                final String startWith = "<br><br>" +
                        new SimpleDateFormat("hh:mm").format(new Date()) + "  ";

                if (TextUtils.isEmpty(color)) {
                    Global.report.append(startWith + append);
                } else {
                    Global.report.append(startWith + ": <font color='"
                            + color + "'>" + append + "</font>");
                }
                // TODO always append? what about overflow?

                send_tv_report.setText(Html.fromHtml(Global.report.toString()));

                // scroll to bottom after appending stuff
                send_sv_report.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
    }


    private void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

}
