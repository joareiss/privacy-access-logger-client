package de.pure.logger.controller.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import de.pure.logger.Global;
import de.pure.logger.R;
import de.pure.logger.controller.AppInfoAdapter;
import de.pure.logger.controller.Logger;
import de.pure.logger.data.DbCom;
import de.pure.logger.entity.AllApplications;
import de.pure.logger.entity.AppInfo;
import de.pure.logger.entity.AppInfoListItem;
import de.pure.logger.util.Utils;

import java.util.ArrayList;
import java.util.Collections;


public class WhatToLogActivity extends BaseActivity {


    private enum FilterApplicationType {
        ALL, PREINSTALLED, NOT_PREINSTALLED;
    }

    private static FilterApplicationType filterApplicationType;

    private static String filterPackageName;

    private static String filterAppName;

    private static AllApplications allApps;

    AppInfoAdapter appInfoAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.whattolog);
        initButtons();

        // init "un/mark all" checkbox
        CheckBox cb_toggle_mark = (CheckBox) findViewById(R.id.whattolog_cb_togglemark);
        cb_toggle_mark.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton,
                                         boolean newValue) {
                if (appInfoAdapter != null) {
                    for (AppInfoListItem item : appInfoAdapter.values) {
                        item.checked = newValue;
                    }
                    appInfoAdapter.notifyDataSetChanged();
                }
            }
        });

        filterPackageName = getPackageNameFilter();
        filterAppName = getAppNameFilter();
        filterApplicationType = FilterApplicationType.ALL;

        if (allApps == null
                || allApps.preinstalled == null
                || allApps.preinstalled.size() < 1) {
            new GetAppsFromDeviceAndPopulateListViewAsyncTask(mActivity).execute();
        } else {
            new PopulateListViewAsyncTask(mActivity).execute();
        }

        initSpinner();

        Global.logcatDebug("AppInfos:");
        for (AppInfo appInfo :
                DbCom.getAllAppInfos(DbCom.getDatabase(mActivity))) {
            Global.logcatDebug(appInfo.toString());
        }
    }


    private void initSpinner() {
        Spinner whattolog_sp_filter = (Spinner) findViewById(R.id.whattolog_sp_filter);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.whattolog_sp_filter, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        whattolog_sp_filter.setAdapter(adapter);
        whattolog_sp_filter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {
                if (filterApplicationType.ordinal() != pos) {
                    filterApplicationType = FilterApplicationType.values()[pos];
//                    String s = (String) parent.getItemAtPosition(pos);
//                    Toast.makeText(mActivity, s + " pos:"
//                            + filterApplicationType, Toast.LENGTH_SHORT).show();
                    new PopulateListViewAsyncTask(mActivity).execute();
                }

            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void initButtons() {

        Button whattolog_btn_refreshlist = (Button) findViewById(R.id.whattolog_btn_refreshlist);
        whattolog_btn_refreshlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                new GetAppsFromDeviceAndPopulateListViewAsyncTask(mActivity).execute();
            }
        });

        Button btn_save = (Button) findViewById(R.id.btn_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                new SyncListWithDbAsyncTask(mActivity).execute();
            }
        });

        ImageButton whattolog_ibtn_applyfilter = (ImageButton) findViewById(R.id.whattolog_ibtn_applyfilter);
        whattolog_ibtn_applyfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                filterPackageName = getPackageNameFilter();
                filterAppName = getAppNameFilter();
                new PopulateListViewAsyncTask(mActivity).execute();
            }
        });
    }

    private void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private String getPackageNameFilter() {
        EditText whattolog_ed_filterpackage = (EditText) findViewById(R.id.whattolog_ed_filterpackage);
        if (whattolog_ed_filterpackage != null) {
            Editable editable = whattolog_ed_filterpackage.getText();
            if (editable != null) {
                return editable.toString();
            }
        }
        return "";
    }

    private String getAppNameFilter() {
        EditText whattolog_ed_filterappname = (EditText) findViewById(R.id.whattolog_ed_filterappname);
        if (whattolog_ed_filterappname != null) {
            Editable editable = whattolog_ed_filterappname.getText();
            if (editable != null) {
                return editable.toString();
            }
        }
        return "";
    }


    private class GetAppsFromDeviceAndPopulateListViewAsyncTask extends AsyncTask<Void, Void, Void> {

        private ProgressDialog dialog;

        private Context mContext;

        private ArrayList<AppInfoListItem> resultList;
        private ArrayList<AppInfo> filteredAppInfos;

        public GetAppsFromDeviceAndPopulateListViewAsyncTask(Context context) {
            mContext = context;
            dialog = new ProgressDialog(context);
            dialog.setCancelable(false);
        }


        @Override
        protected void onPreExecute() {
            this.dialog.setMessage(mContext.getString(R.string.whattolog_dlg_loading));
            this.dialog.show();
        }

        @Override
        protected void onPostExecute(Void v) {
            if (resultList != null) {
                ListView whattolog_lv_appinfos = (ListView)
                        findViewById(R.id.whattolog_lv_appinfos);

                AppInfoListItem[] values = resultList.toArray(
                        new AppInfoListItem[filteredAppInfos.size()]);
                appInfoAdapter = new AppInfoAdapter(mActivity, values);
                whattolog_lv_appinfos.setAdapter(appInfoAdapter);
                appInfoAdapter.notifyDataSetChanged();
            }
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {

            allApps = Utils.getAllApplications(getPackageManager());


            // add all filteredAppInfos to ListView
            // but set their checkbox to checked if found in DB
            resultList = new ArrayList<AppInfoListItem>();

            ArrayList<AppInfo> appInfosFromDb = DbCom.
                    getAllAppInfos(DbCom.getDatabase(mActivity));

            filteredAppInfos = getFilteredAppInfos();

            for (AppInfo filteredAppInfo : filteredAppInfos) {
                AppInfoListItem toAdd = new AppInfoListItem();
                toAdd.appName = filteredAppInfo.appName;
                toAdd.packageName = filteredAppInfo.packageName;
                if (appInfosFromDb.contains(filteredAppInfo)) {
                    toAdd.checked = true;
                } else {
                    toAdd.checked = false;
                }
                resultList.add(toAdd);
            }
            return null;
        }
    }


    private class PopulateListViewAsyncTask extends AsyncTask<Void, Void, Void> {

        private ProgressDialog dialog;

        private Context mContext;

        private ArrayList<AppInfoListItem> resultList;
        private ArrayList<AppInfo> filteredAppInfos;

        public PopulateListViewAsyncTask(Context context) {
            mContext = context;
            dialog = new ProgressDialog(context);
            dialog.setCancelable(false);
        }


        @Override
        protected void onPreExecute() {
            this.dialog.setMessage(mContext.getString(R.string.whattolog_dlg_loading));
            this.dialog.show();
        }

        @Override
        protected void onPostExecute(Void v) {
            if (resultList != null) {
                ListView whattolog_lv_appinfos = (ListView) findViewById(R.id.whattolog_lv_appinfos);

                AppInfoListItem[] values = resultList.toArray(
                        new AppInfoListItem[filteredAppInfos.size()]);
                appInfoAdapter = new AppInfoAdapter(mActivity, values);
                whattolog_lv_appinfos.setAdapter(appInfoAdapter);
                appInfoAdapter.notifyDataSetChanged();
            }
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {


            // add all filteredAppInfos to ListView
            // but set their checkbox to checked if found in DB
            resultList = new ArrayList<AppInfoListItem>();

            ArrayList<AppInfo> appInfosFromDb = DbCom.
                    getAllAppInfos(DbCom.getDatabase(mActivity));

            filteredAppInfos = getFilteredAppInfos();

            for (AppInfo filteredAppInfo : filteredAppInfos) {
                AppInfoListItem toAdd = new AppInfoListItem();
                toAdd.appName = filteredAppInfo.appName;
                toAdd.packageName = filteredAppInfo.packageName;
                if (appInfosFromDb.contains(filteredAppInfo)) {
                    toAdd.checked = true;
                } else {
                    toAdd.checked = false;
                }
                resultList.add(toAdd);
            }
            return null;
        }
    }


    private ArrayList<AppInfo> getFilteredAppInfos() {
        if (allApps == null
                || allApps.preinstalled == null
                || allApps.notpreinstalled == null
                || allApps.preinstalled.size() < 1) {
            return new ArrayList<AppInfo>();
        }


        ArrayList<AppInfo> listFilteredBySpinner;
        switch (filterApplicationType) {
            case PREINSTALLED:
                listFilteredBySpinner = AppInfo.cloneList(allApps.preinstalled);
                break;
            case NOT_PREINSTALLED:
                listFilteredBySpinner = AppInfo.cloneList(allApps.notpreinstalled);
                break;
            default:
                listFilteredBySpinner = AppInfo.cloneList(allApps.preinstalled);
                // clone the rest if not cloned yet
                for (AppInfo appInfo : allApps.notpreinstalled) {
                    if (!listFilteredBySpinner.contains(appInfo)) {
                        AppInfo ai = new AppInfo(appInfo.appName, appInfo.packageName);
                        listFilteredBySpinner.add(ai);
                    }
                }
                break;
        }

        // if list is empty, then don't do anything else and return
        if (listFilteredBySpinner.size() < 1) {
            return new ArrayList<AppInfo>();
        }


        // if filters are empty, then don't do anything else and return
        if (TextUtils.isEmpty(filterPackageName)
                && TextUtils.isEmpty(filterAppName)) {
            Collections.sort(listFilteredBySpinner);
            return listFilteredBySpinner;
        }


        ArrayList<AppInfo> listFiltered = new ArrayList<AppInfo>();
        // filtered by spinner and TextViews

        for (AppInfo appInfo : listFilteredBySpinner) {
            // add the appInfo if filterPackageName and filterAppName apply

            boolean fitsToPackageFilter = true;
            if (!TextUtils.isEmpty(filterPackageName) &&
                    !Utils.isContainedIgnoreCase(filterPackageName, appInfo.packageName)) {
                fitsToPackageFilter = false;
            }
            boolean fitsToAppFilter = true;
            if (!TextUtils.isEmpty(filterAppName) &&
                    !Utils.isContainedIgnoreCase(filterAppName, appInfo.appName)) {
                fitsToAppFilter = false;
            }

            if (fitsToPackageFilter && fitsToAppFilter) {
                listFiltered.add(appInfo);
            }
        }

        Collections.sort(listFiltered);
        return listFiltered;
    }

    private class SyncListWithDbAsyncTask extends AsyncTask<Void, Void, Void> {

        private ProgressDialog dialog;

        private Context mContext;

        public SyncListWithDbAsyncTask(Context context) {
            mContext = context;
            dialog = new ProgressDialog(context);
            dialog.setCancelable(false);
        }

        @Override
        protected void onPreExecute() {
            this.dialog.setMessage(mActivity.getString(R.string.whattolog_dlg_saving));
            this.dialog.show();
        }

        @Override
        protected void onPostExecute(Void v) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if (syncAppInfosWithDb()) {
                Logger.updateAppInfos(mContext);
            }
            return null;
        }
    }

    /**
     * @return true if changed something in db
     */
    private boolean syncAppInfosWithDb() {
        if (appInfoAdapter == null) {
            return false;
        }

        ArrayList<AppInfo> appInfosFromDb = DbCom.
                getAllAppInfos(DbCom.getDatabase(mActivity));


        AppInfoListItem[] listItems = appInfoAdapter.values;

        boolean changedSomething = false;
        for (AppInfoListItem displayedItem : listItems) {

            AppInfo appInfoDisplayed =
                    new AppInfo(displayedItem.appName, displayedItem.packageName);
            if (appInfosFromDb.contains(appInfoDisplayed)) {
                if (displayedItem.checked) {
                    // already in db and (still) checked
                    // thus do nothing
                } else {
                    // not checked (anymore), but found in db
                    // thus delete it from db
                    DbCom.deleteAppInfo(DbCom.getDatabase(mActivity),
                            appInfoDisplayed.packageName);
                    changedSomething = true;
                }
            } else {
                if (displayedItem.checked) {
                    // not in db and (still) checked
                    // thus write it to db
                    DbCom.writeAppInfo(DbCom.getDatabase(mActivity), appInfoDisplayed);
                    changedSomething = true;
                } else {
                    // not checked (anymore) and not found in db
                    // thus do nothing
                }
            }
        }


        return changedSomething;
    }

}
