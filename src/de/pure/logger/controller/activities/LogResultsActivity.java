package de.pure.logger.controller.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import au.com.bytecode.opencsv.CSVWriter;
import de.pure.logger.Global;
import de.pure.logger.R;
import de.pure.logger.data.DbCom;
import de.pure.logger.entity.XPrivacyLogEntry;
import de.pure.logger.util.Utils;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;


public class LogResultsActivity extends BaseActivity {


    private String filterText;

    private Activity mActivity;

    //    private static ArrayList<LogEntry> currentlyDisplayedLogEntries;
    private static ArrayList<XPrivacyLogEntry> currentlyDisplayedLogEntries;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.logresults);
        mActivity = this;
        filterText = "";
        initButtons();
        getPackageNameFilter();

        // TODO: always get from DB ?
        if (currentlyDisplayedLogEntries == null
                || currentlyDisplayedLogEntries.size() < 1) {
            new PopulateDbListViewAsyncTask(mActivity).execute();
        } else {
            populateListView(getFilteredLogEntries());
        }


    }


    private void initButtons() {

        Button logresults_btn_refresh = (Button) findViewById(
                R.id.logresults_btn_refresh);
        logresults_btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                new PopulateDbListViewAsyncTask(mActivity).execute();
            }
        });


        final Button logresults_btn_cleardb = (Button) findViewById(
                R.id.logresults_btn_cleardb);
        logresults_btn_cleardb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(mActivity)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(R.string.logresults_dlg_cleartitle)
                        .setMessage(R.string.logresults_dlg_clearmsg)
                        .setPositiveButton(R.string.dlg_btn_yes,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        new ClearDbAsyncTask(mActivity).execute();
                                    }
                                })
                        .setNegativeButton(R.string.dlg_btn_no, null)
                        .show();
            }
        });

        final Button logresults_btn_export = (Button) findViewById(
                R.id.logresults_btn_export);
        logresults_btn_export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(mActivity)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(R.string.logresults_dlg_exporttitle)
                        .setMessage(R.string.logresults_dlg_exportmsg)
                        .setPositiveButton(R.string.dlg_btn_yes,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        new ExportDatabaseCSVTask(mActivity).execute();
                                    }
                                })
                        .setNegativeButton(R.string.dlg_btn_no, null)
                        .show();
            }
        });

        ImageButton logresults_ibtn_applyfilter = (ImageButton)
                findViewById(R.id.logresults_ibtn_applyfilter);
        logresults_ibtn_applyfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                filterText = getPackageNameFilter();
                populateListView(getFilteredLogEntries());
            }
        });

    }


    private void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private String getPackageNameFilter() {
        EditText logresults_ed_package = (EditText) findViewById(
                R.id.logresults_ed_package);
        if (logresults_ed_package != null) {
            Editable editable = logresults_ed_package.getText();
            if (editable != null) {
                return editable.toString();
            }
        }
        return "";
    }


    private class LogEntryAdapter extends ArrayAdapter<XPrivacyLogEntry> {
        private final Context context;
        private final XPrivacyLogEntry[] values;

        public LogEntryAdapter(Context context, XPrivacyLogEntry[] values) {
            super(context, R.layout.logresultslistitem, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inf = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            // use convertView to avoid OutOfMemory
            // http://stackoverflow.com/a/11388381
            View row = convertView;
            if (row == null) {
                row = inf.inflate(R.layout.logresultslistitem, parent, false);
            }

            final XPrivacyLogEntry entr = values[position];

            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    new AlertDialog.Builder(mActivity)
                            .setTitle("Log entry")
                            .setMessage(entr.toString())
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            });

            TextView logresultslistitem_tv_creationdate = (TextView) row.findViewById(
                    R.id.logresultslistitem_tv_creationdate);
            TextView logresultslistitem_tv_packagename = (TextView) row.findViewById(
                    R.id.logresultslistitem_tv_packagename);
            TextView logresultslistitem_tv_content = (TextView) row.findViewById(
                    R.id.logresultslistitem_tv_content);

            if (logresultslistitem_tv_creationdate != null) {
                logresultslistitem_tv_creationdate.setText("" +
                        new SimpleDateFormat("hh:mm").
                                format(entr.creationDate));
            }
            String pname = "";
            pname += entr.packageName;
            //  pname += " " + entr.uid;
            if (logresultslistitem_tv_packagename != null) {
                logresultslistitem_tv_packagename.setText(pname);
            }
            if (logresultslistitem_tv_content != null) {
                logresultslistitem_tv_content.setText("" + entr.content);
            }


            return row;
        }

    }


    private class ClearDbAsyncTask extends AsyncTask<Void, Void, Void> {

        private ProgressDialog dialog;

        private Context mActivity;

        AsyncTask self;

        public ClearDbAsyncTask(Context context) {
            self = this;
            mActivity = context;
            dialog = new ProgressDialog(context);
            dialog.setCancelable(true);
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    self.cancel(true);
                }
            });
        }


        @Override
        protected void onPreExecute() {
            this.dialog.setMessage(mActivity.getString(R.string.logresults_dlg_deleting));
            this.dialog.show();
        }

        @Override
        protected void onPostExecute(Void v) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            final int size = DbCom.getAllXPrivacyLogEntries(DbCom.getDatabase(mActivity), 2).size();
            if (size > 0) {
                Toast.makeText(mActivity,
                        "Cleared database did not work." +
                                " There are still " + size
                                + " entries in the database...",
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(mActivity, "Cleared database worked!",
                        Toast.LENGTH_SHORT).show();
            }

            new PopulateDbListViewAsyncTask(mActivity).execute();

        }

        @Override
        protected Void doInBackground(Void... voids) {
            DbCom.clearLogs(DbCom.getDatabase(mActivity));
            return null;
        }
    }


    private class PopulateDbListViewAsyncTask extends AsyncTask<Void, Void, Void> {

        private ProgressDialog dialog;

        private Context mActivity;

        private AsyncTask self;

        public PopulateDbListViewAsyncTask(Context context) {
            self = this;
            mActivity = context;
            dialog = new ProgressDialog(context);
            dialog.setCancelable(true);
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    self.cancel(true);
                }
            });
        }


        @Override
        protected void onPreExecute() {
            this.dialog.setMessage(mActivity.getString(R.string.logresults_dlg_loading));
            this.dialog.show();
        }

        @Override
        protected void onPostExecute(Void v) {
            populateListView(getFilteredLogEntries());
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            currentlyDisplayedLogEntries = DbCom.getAllXPrivacyLogEntries(
                    DbCom.getInstance(mActivity).openDatabase(), 500);
            return null;
        }
    }

    private ArrayList<XPrivacyLogEntry> getFilteredLogEntries() {
        if (currentlyDisplayedLogEntries == null
                || currentlyDisplayedLogEntries.size() < 1) {
            return new ArrayList<XPrivacyLogEntry>();
        }

        if (TextUtils.isEmpty(filterText)) {
            return currentlyDisplayedLogEntries;
        }

        ArrayList<XPrivacyLogEntry> filtered = new ArrayList<XPrivacyLogEntry>();

        for (XPrivacyLogEntry le : currentlyDisplayedLogEntries) {
            if (Utils.isContainedIgnoreCase(filterText, le.packageName)) {
                filtered.add(le);
            } else if (Utils.isContainedIgnoreCase(filterText, "" + le.uid)) {
                filtered.add(le);
            } else if (Utils.isContainedIgnoreCase(filterText, "" + le.content)) {
                filtered.add(le);
            }
        }

        return filtered;
    }


    private void populateListView(ArrayList<XPrivacyLogEntry> logentries) {
        ListView db_logentry_listview = (ListView) findViewById(
                R.id.db_logentry_listview);
        XPrivacyLogEntry[] values = logentries.toArray(new XPrivacyLogEntry[logentries.size()]);
        LogEntryAdapter adapter = new LogEntryAdapter(mActivity, values);
        db_logentry_listview.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


    private class ExportDatabaseCSVTask extends AsyncTask<String, Void, Boolean> {
        private ProgressDialog dialog;

        private Context mActivity;

        private AsyncTask self;

        public ExportDatabaseCSVTask(Context context) {
            self = this;
            mActivity = context;
            dialog = new ProgressDialog(mActivity);
            dialog.setCancelable(true);
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    self.cancel(true);
                }
            });
        }

        @Override
        protected void onPreExecute() {
            this.dialog.setMessage(mActivity.getString(
                    R.string.logresults_dlg_exporting));
            this.dialog.show();
        }

        protected Boolean doInBackground(final String... args) {

            ArrayList<XPrivacyLogEntry> allLogEntries = DbCom.getAllXPrivacyLogEntries(
                    DbCom.getDatabase(mActivity), -1);

            if (allLogEntries.size() < 1) {
                return true; // regarding exporting nothing as success..
            }

            File file = new File(Environment.getExternalStorageDirectory(),
                    Global.EXPORT_CSVFILE);
            try {
                file.createNewFile();
                CSVWriter csvWrite = new CSVWriter(new FileWriter(file));

                for (XPrivacyLogEntry logEntry : allLogEntries) {
                    csvWrite.writeNext(logEntry.toStringArray());
                }

                csvWrite.close();
                return true;
            } catch (Exception e) {
                Global.logcatError(e.getLocalizedMessage());
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }
            if (success) {
                Toast.makeText(mActivity, "Export successful!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mActivity, "Export failed!", Toast.LENGTH_SHORT).show();
            }
        }
    }


}
