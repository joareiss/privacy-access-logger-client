package de.pure.logger.controller;

import android.content.Context;
import android.os.FileObserver;
import de.pure.logger.Global;
import de.pure.logger.entity.XPrivacyLogEntry;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * User: joachim
 * Date: 3/8/14
 * Time: 12:04 PM
 */
public class LoggingFileObserver extends FileObserver {

    private Context mContext;

    private String mFileName;

    public LoggingFileObserver(Context context, String path) {
        super(path);
        mFileName = path;
        mContext = context;
    }

    public void close() {
        super.finalize();
    }

    @Override
    public void onEvent(int event, String path) {

        String eventName = "?";
        boolean logIt = false;
        switch (event) {
            case FileObserver.ACCESS:
                eventName = "ACCESS";
                logIt = true;
                break;
            case FileObserver.ATTRIB:
                eventName = "ATTRIB";
                break;
            case FileObserver.CLOSE_NOWRITE:
                eventName = "CLOSE_NOWRITE";
                break;
            case FileObserver.CLOSE_WRITE:
                eventName = "CLOSE_WRITE";
                break;
            case FileObserver.OPEN:
                eventName = "OPEN";
                logIt = true;
                break;
            case FileObserver.CREATE:
                eventName = "CREATE";
                break;
            case FileObserver.DELETE:
                eventName = "DELETE";
                break;
            case FileObserver.DELETE_SELF:
                eventName = "DELETE_SELF";
                break;
            case FileObserver.MODIFY:
                eventName = "MODIFY";
                logIt = true;
                break;
            case FileObserver.MOVED_FROM:
                eventName = "MOVED_FROM";
                logIt = true;
                break;
            case FileObserver.MOVED_TO:
                eventName = "MOVED_TO";
                logIt = true;
                break;
            case FileObserver.MOVE_SELF:
                eventName = "MOVE_SELF";
                break;
            default:
                break;
        }

        if (!eventName.equals("?")) {
//            Global.log( "FILEWATCH EVENT ++++++++++++++++++++++ " + event + "==" +
//                    eventName + "    " + path);

            String filename = path != null ? path : mFileName;
            final String content = "File '" + filename + "'\n" +
                    "access : " + eventName;
            XPrivacyLogEntry entry = new XPrivacyLogEntry(
                    Global.FILEWATCH_LOG_PACKAGENAME, Global.INVALID_UID, content, new Date());
            Logger.log(mContext, entry);
        }

    }


    /**
     * TODO should we use this?
     *
     * @param file
     * @return
     */
    private String readLastLineOfTextFile(File file) {
        return readLastLinesOfTextFile(file, 1);
    }

    private String readLastLinesOfTextFile(File file, int numberOfLastLines) {
        java.io.RandomAccessFile fileHandler = null;
        try {
            fileHandler = new java.io.RandomAccessFile(file, "r");
            long fileLength = fileHandler.length() - 1;
            StringBuilder sb = new StringBuilder();
            int line = 0;

            for (long filePointer = fileLength; filePointer != -1; filePointer--) {
                fileHandler.seek(filePointer);
                int readByte = fileHandler.readByte();

                if (readByte == 0xA) {
                    if (line == numberOfLastLines) {
                        if (filePointer == fileLength) {
                            continue;
                        } else {
                            break;
                        }
                    }
                } else if (readByte == 0xD) {
                    line = line + 1;
                    if (line == numberOfLastLines) {
                        if (filePointer == fileLength - 1) {
                            continue;
                        } else {
                            break;
                        }
                    }
                }
                sb.append((char) readByte);
            }

            sb.deleteCharAt(sb.length() - 1);
            String lastLine = sb.reverse().toString();
            return lastLine;
        } catch (java.io.FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (fileHandler != null)
                try {
                    fileHandler.close();
                } catch (IOException e) {
                /* ignore */
                }
        }
    }

}
