package de.pure.logger.util;

import java.util.List;

import android.os.AsyncTask;
import android.util.Log;
import eu.chainfire.libsuperuser.Shell.SU;

public class SuTask extends AsyncTask<String, String, Void> {
    public static final String TAG = "SuTask";
    
    @Override
    protected Void doInBackground(String... params) {
        for (String command : params) {
            List<String> output = SU.run(command);
            if (output != null) {
                StringBuilder sb = new StringBuilder();
                for (String line : output) {
                    sb.append(line);
                }
                publishProgress(sb.toString());
            }
        }
        return null;
    }
    
    @Override
    protected void onProgressUpdate(String... values) {
        Log.d(TAG, values[0]);
    }
}