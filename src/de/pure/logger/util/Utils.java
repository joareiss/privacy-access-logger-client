package de.pure.logger.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import de.pure.logger.Global;
import de.pure.logger.entity.AllApplications;
import de.pure.logger.entity.AppInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Utils {


    /**
     * Something like this would be sent by XPrivacy
     * <p/>
     * For testing purposes
     */
    public static void sendFakeLogEvent(Context context) {
        Intent intent = new Intent();
        intent.setAction(Global.ACTION_XPRIVACY_LOG_NOTIFICATION);
        intent.putExtra("packageName", Global.FAKE_LOG_PACKAGENAME);
        intent.putExtra("uid", new Random().nextInt(1200));
        intent.putExtra("content", randomString(10));
        intent.putExtra("creationDate", System.currentTimeMillis());
        context.sendBroadcast(intent);
    }

    private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static Random rnd = new Random();

    private static String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }


    /**
     * @param packageManager the PackageManager to get the applications from
     * @return AllApplications
     */
    public static AllApplications getAllApplications(PackageManager packageManager) {

        AllApplications result = new AllApplications();
        result.preinstalled = new ArrayList<AppInfo>();
        result.notpreinstalled = new ArrayList<AppInfo>();

        List<ApplicationInfo> installedApplications = packageManager
                .getInstalledApplications(PackageManager.GET_META_DATA);
        for (ApplicationInfo applicationInfo : installedApplications) {
            if (isApplicationPreInstalled(applicationInfo)) {
                result.preinstalled.add(new AppInfo(packageManager, applicationInfo));
            } else {
                result.notpreinstalled.add(new AppInfo(packageManager, applicationInfo));
            }
        }

        return result;
    }


    private static boolean isApplicationPreInstalled(ApplicationInfo applicationInfo) {

//        // Check if package is system app
//        if ((applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) {
//            return true;
//        }
//        return false;
        try {
            if (applicationInfo != null) {
                if ((applicationInfo.flags & 1) == 1) {
                    /*
                    If flags is an uneven number, then it
                    is a preinstalled application, because in that case
                    ApplicationInfo.FLAG_SYSTEM ( == 0x00000001 )
                    is added to flags
                    */
                    return true;

                }
            }
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        }
        return false;
    }


    /**
     * ignore case sensitivity when checking for String.contains() ..
     *
     * @param check
     * @param against
     * @return
     */
    public static boolean isContainedIgnoreCase(String check, String against) {
        if (against == null || check == null) {
            return false;
        }
        if (against.contains(check)) {
            return true;
        }
        return against.toLowerCase().contains(check.toLowerCase());
    }


}
