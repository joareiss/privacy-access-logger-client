package de.pure.logger.entity;

import java.util.ArrayList;

public class AllApplications {

    public ArrayList<AppInfo> preinstalled;
    public ArrayList<AppInfo> notpreinstalled;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AllApplications)) return false;

        AllApplications that = (AllApplications) o;

        if (!notpreinstalled.equals(that.notpreinstalled)) return false;
        if (!preinstalled.equals(that.preinstalled)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = preinstalled.hashCode();
        result = 31 * result + notpreinstalled.hashCode();
        return result;
    }
}
