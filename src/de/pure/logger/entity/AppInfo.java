package de.pure.logger.entity;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import java.util.ArrayList;

/**
 * Lightweight POJO for all we care about
 */
public class AppInfo implements Comparable<AppInfo> {

    public String appName;
    public String packageName;

    public AppInfo(PackageManager packageManager, ApplicationInfo applicationInfo) {
        this.packageName = applicationInfo.packageName;
        CharSequence appLabel = packageManager.getApplicationLabel(applicationInfo);
        if (appLabel != null) {
            this.appName = appLabel.toString();
        } else {
            // just show the package name as app name then
            this.appName = this.packageName;
        }
    }

    public AppInfo(String appName, String packageName) {
        this.appName = appName;
        this.packageName = packageName;
    }

    public AppInfo() {
    }


    /**
     * Deep copy of a list of AppInfo
     */
    public static ArrayList<AppInfo> cloneList(ArrayList<AppInfo> appInfoList) {
        ArrayList<AppInfo> clonedList = new ArrayList<AppInfo>(appInfoList.size());
        for (AppInfo ai : appInfoList) {
            clonedList.add(new AppInfo(ai.appName, ai.packageName));
        }
        return clonedList;
    }


    @Override
    public String toString() {
//        return "AppInfo{" +
//                "appName='" + appName + '\'' +
//                ", packageName='" + packageName + '\'' +
//                '}';
        return packageName;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (!(that instanceof AppInfo)) return false;

        AppInfo appInfo = (AppInfo) that;

        if (appName != null ? !appName.equals(appInfo.appName) : appInfo.appName != null) return false;
        if (packageName != null ? !packageName.equals(appInfo.packageName) : appInfo.packageName != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = appName != null ? appName.hashCode() : 0;
        result = 31 * result + (packageName != null ? packageName.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(AppInfo appInfo) {
        return this.appName.compareToIgnoreCase(appInfo.appName);
    }
}
