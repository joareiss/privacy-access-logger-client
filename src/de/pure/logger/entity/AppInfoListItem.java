package de.pure.logger.entity;

public class AppInfoListItem {

    public String appName;
    public String packageName;
    public boolean checked;

    public AppInfoListItem(String appName, String packageName, boolean checked) {
        this.appName = appName;
        this.packageName = packageName;
        this.checked = checked;
    }

    public AppInfoListItem() {
    }

    @Override
    public String toString() {
        return "AppInfoListItem{" +
                "appName='" + appName + '\'' +
                ", packageName='" + packageName + '\'' +
                ", checked=" + checked +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AppInfoListItem that = (AppInfoListItem) o;

        if (checked != that.checked) return false;
        if (appName != null ? !appName.equals(that.appName) : that.appName != null) return false;
        if (packageName != null ? !packageName.equals(that.packageName) : that.packageName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = appName != null ? appName.hashCode() : 0;
        result = 31 * result + (packageName != null ? packageName.hashCode() : 0);
        result = 31 * result + (checked ? 1 : 0);
        return result;
    }
}
