package de.pure.logger.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.os.Bundle;

/**
 * POJO with Comparable by creationDate for some ordered Collection
 */
public class XPrivacyLogEntry implements Comparable<XPrivacyLogEntry> {

    public Date creationDate;
    public String packageName;
    public int uid;
    public String content;

    public XPrivacyLogEntry() {
        this.packageName = "";
        this.uid = -1;
        this.content = "";
        this.creationDate = new Date();
    }

    public XPrivacyLogEntry(String packageName, int uid, String content, Date creationDate) {
        this.packageName = packageName;
        this.uid = uid;
        this.content = content;
        this.creationDate = creationDate;
    }

    public static XPrivacyLogEntry fromBundle(Bundle bundle) {
        return new XPrivacyLogEntry(
                bundle.getString("packageName"), 
                bundle.getInt("uid"), 
                bundle.getString("content"), 
                new Date(bundle.getLong("creationDate")));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        XPrivacyLogEntry that = (XPrivacyLogEntry) o;

        if (uid != that.uid)
            return false;
        if (content != null ? !content.equals(that.content) : that.content != null)
            return false;
        if (packageName != null ? !packageName.equals(that.packageName) : that.packageName != null)
            return false;

        if (creationDate != null ? !creationDate.equals(that.creationDate)
                : that.creationDate != null)
            return false;

        // equal if same minute in time
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
        String thisDate = sdf.format(creationDate);
        String thatDate = sdf.format(that.creationDate);
        if (!thisDate.equals(thatDate)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = creationDate != null ? creationDate.hashCode() : 0;
        result = 31 * result + (packageName != null ? packageName.hashCode() : 0);
        result = 31 * result + uid;
        result = 31 * result + (content != null ? content.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(XPrivacyLogEntry that) {
        return this.creationDate.compareTo(that.creationDate);
    }

    @Override
    public String toString() {
        return "XPrivacyLogEntry{" + "creationDate=" + creationDate + ", packageName='"
                + packageName + '\'' + ", uid=" + uid + ", content='" + content + '\'' + '}';
    }

    public String[] toStringArray() {
        String arrStr[] = { getDateString(creationDate), packageName, "" + uid, "" + content };
        return arrStr;
    }

    private static String getDateString(Date date) {
        if (date != null) {
            return new SimpleDateFormat("yyyy-MM-dd hh:mm").format(date);
        }
        return "";
    }
}
