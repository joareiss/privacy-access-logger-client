/**
 * Background service to spool nflog command output
 *
 * Copyright (C) 2013 Kevin Cernekee
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Kevin Cernekee
 * @version 1.0
 */

package de.pure.logger.network;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import de.pure.logger.Global;
import de.pure.logger.controller.Logger;
import de.pure.logger.entity.XPrivacyLogEntry;
import eu.chainfire.libsuperuser.Shell;
import eu.chainfire.libsuperuser.StreamGobbler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class NFLogService extends Service {

    public static final String TAG = "NFLogService";

    public static String nflog;

    private final IBinder mBinder = new Binder();

    private Shell.Interactive outputShell;

    private List<String> observedProtocols = Arrays.asList("TCP", "UDP");

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public void onCreate() {
        final Context context = getApplicationContext();
        nflog = InstallUtils.getNflog().getFile(context).getAbsolutePath();
        outputShell = new Shell.Builder().useSU().setMinimalLogging(true)
                .setOnSTDOUTLineListener(new StreamGobbler.OnLineListener() {
                    private PackageManager pm = getPackageManager();

                    @Override
                    public void onLine(String log) {
                        // Log.d(TAG, log);
                        String[] parts = log.split(" ");
                        // discard logs not related to Privacy Logger
                        if (!parts[0].equals(IPTables.NFLOG_PREFIX)) {
                            return;
                        }
                        Map<String, String> values = new HashMap<String, String>();
                        for (int i = 1; i < parts.length; i++) {
                            String[] entry = parts[i].split("=", 2);
                            values.put(entry[0], entry[1]);
                        }
                        String proto = values.get("PROTO");
                        if (!observedProtocols.contains(proto)) {
                            return;
                        }
                        String content;
                        Integer uid = Global.INVALID_UID;
                        String pkg = Global.NETWORK_LOG_PACKAGENAME;
                        String in = parts[1].substring(3);
                        String out = parts[2].substring(4);
                        if (in.isEmpty()) {
                            // OUTPUT
                            String dst = values.get("DST");
                            String dpt = values.get("DPT");
                            try {
                                uid = Integer.valueOf(values.get("UID"));
                            } catch (NumberFormatException e) {
                            }
                            content = String.format("OUT %s %s %s:%s", out, proto, dst, dpt);
                        } else {
                            // INPUT
                            String src = values.get("SRC");
                            String spt = values.get("SPT");
                            String dpt = values.get("DPT");
                            if (dpt != null) {
                                uid = findUID(proto, Integer.parseInt(dpt));
                            }
                            content = String.format("IN %s %s %s:%s", in, proto, src, spt);
                        }
                        if (!uid.equals(Global.INVALID_UID)) {
                            pkg = pm.getNameForUid(uid);
                        }
                        Log.d(TAG, content + " " + uid + "/" + pkg);
                        XPrivacyLogEntry xPrivacyLogEntry = new XPrivacyLogEntry(pkg, uid, content,
                                new Date());
                        Logger.log(context, xPrivacyLogEntry);
                    }
                }).open(new Shell.OnCommandResultListener() {
                    public void onCommandResult(int commandCode, int exitCode, List<String> output) {
                        if (exitCode != 0) {
                            Log.e(TAG, "Can't start NFLog shell: exitCode " + exitCode);
                            stopSelf();
                        } else {
                            Log.i(TAG, "NFLog shell started");
                            outputShell.addCommand(nflog + " " + IPTables.NFLOG_GROUP);
                        }
                    }
                });
    }

    private Integer findUID(String proto, int port) {
        // TODO: proper handling for UDP packets
        try {
            BufferedReader reader = new BufferedReader(new FileReader("/proc/net/"
                    + proto.toLowerCase() + "6"));
            String line = reader.readLine();
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split("\\s+");
                if (port == Integer.parseInt(parts[2].substring(33), 16)) {
                    reader.close();
                    return Integer.parseInt(parts[8]);
                }
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Global.INVALID_UID;
    }

    public void onDestroy() {
        Log.e(TAG, "Received request to kill NFLogService");
        /* FIXME: not closing the shell through libsuperuser yet */
    }
}
