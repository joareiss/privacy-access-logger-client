package de.pure.logger.network;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import de.pure.logger.R;

public class InstallUtils {

    public enum Binary {
        BUSYBOX_ARM("busybox", "b7cc13858f3fd50abe463abb37b2a4f1", R.raw.busybox_arm),
        BUSYBOX_MIPS("busybox", "3926ad0fda956e8d2f12a1a761525ef4", R.raw.busybox_mips),
        BUSYBOX_X86("busybox", "93aa4283b0aca89e9c3b377264dabc51", R.raw.busybox_x86),
        IPTABLES_ARM("iptables", "ee9e742dd608a6e1632263a58b457e75", R.raw.iptables_arm),
        IPTABLES_MIPS("iptables", "d011771b13ce5b1e5cd20f65b0844662", R.raw.iptables_mips),
        IPTABLES_X86("iptables", "5637f57e80d128205ad6bca2cc20974a", R.raw.iptables_x86),
        NFLOG_ARM("nflog", "b27b06f94725f9f9fc5fc9638c9b7d48", R.raw.nflog_arm),
        NFLOG_MIPS("nflog", "9e5e4e6cc7e3596cfba2e1ea3d0f2339", R.raw.nflog_mips),
        NFLOG_X86("nflog", "a9e04bae5149c772596507f9712a6bf0", R.raw.nflog_x86);

        private String filename;
        private String md5sum;
        private int rawId;

        private Binary(String filename, String md5sum, int rawId) {
            this.filename = filename;
            this.md5sum = md5sum;
            this.rawId = rawId;
        }

        public File getFile(Context context) {
            return new File(context.getFilesDir(), filename);
        }

        public String getMd5sum() {
            return md5sum;
        }

        public int getRawId() {
            return rawId;
        }
    }

    private static final String TAG = "InstallUtils";

    private static Binary busybox;
    private static Binary iptables;
    private static Binary nflog;

    private static void initBinaries() {
        String abi = Build.CPU_ABI;
        if (abi.startsWith("x86")) {
            busybox = Binary.BUSYBOX_X86;
            iptables = Binary.IPTABLES_X86;
            nflog = Binary.NFLOG_X86;
        } else if (abi.startsWith("mips")) {
            busybox = Binary.BUSYBOX_MIPS;
            iptables = Binary.IPTABLES_MIPS;
            nflog = Binary.NFLOG_MIPS;
        } else {
            busybox = Binary.BUSYBOX_ARM;
            iptables = Binary.IPTABLES_ARM;
            nflog = Binary.NFLOG_ARM;
        }
    }

    public static boolean installBinaries(Context context) {
        initBinaries();
        return installBinary(context, busybox) && installBinary(context, iptables)
                && installBinary(context, nflog);
    }

    private static boolean installBinary(Context context, Binary binary) {
        try {
            File binaryFile = binary.getFile(context);
            if (binaryFile.exists() && md5sum(binaryFile).equals(binary.getMd5sum())) {
                return true;
            }
            binaryFile.delete();
            final InputStream in = context.getResources().openRawResource(binary.getRawId());
            final FileOutputStream out = new FileOutputStream(binaryFile);
            byte buf[] = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();
            binaryFile.setReadable(true, false);
            binaryFile.setExecutable(true, false);
            return true;
        } catch (Exception e) {
            Log.e(TAG, "Installing binary failed: " + e.getLocalizedMessage());
            return false;
        }
    }

    private static String md5sum(File file) {
        try {
            InputStream is = new FileInputStream(file);
            DigestInputStream dis = new DigestInputStream(is, MessageDigest.getInstance("MD5"));
            byte buf[] = new byte[1024];
            while (dis.read(buf) > 0) {
            }
            dis.close();
            byte[] digest = dis.getMessageDigest().digest();

            String hash = "";
            for (int i = 0; i < digest.length; i++) {
                hash += Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1);
            }
            return hash;
        } catch (Exception e) {
            return null;
        }
    }

    public static Binary getBusybox() {
        if (busybox == null) {
            initBinaries();
        }
        return busybox;
    }

    public static Binary getIptables() {
        if (iptables == null) {
            initBinaries();
        }
        return iptables;
    }

    public static Binary getNflog() {
        if (nflog == null) {
            initBinaries();
        }
        return nflog;
    }
}
