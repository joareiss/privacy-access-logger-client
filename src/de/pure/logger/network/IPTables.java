package de.pure.logger.network;

import de.pure.logger.util.SuTask;
import android.content.Context;

public class IPTables {

    // private static final String TAG = "IPTables";

    public static final String NFLOG_PREFIX = "{PALC}";
    
    public static final int NFLOG_GROUP = 40;
    
    public static final String RULE_SPEC = " -j NFLOG --nflog-prefix \"" + NFLOG_PREFIX + "\" --nflog-group " + NFLOG_GROUP;

    

    public static void addRules(Context context) {
        String iptables = InstallUtils.getIptables().getFile(context).getAbsolutePath();
        String input = iptables + " -A INPUT" + RULE_SPEC;
        String output = iptables + " -A OUTPUT" + RULE_SPEC;
        new SuTask().execute(input, output);
    }
    
    public static void deleteAndAddRules(Context context) {
        String iptables = InstallUtils.getIptables().getFile(context).getAbsolutePath();
        String deleteInput = iptables + " -D INPUT" + RULE_SPEC;
        String deleteOutput = iptables + " -D OUTPUT" + RULE_SPEC;
        String addInput = iptables + " -A INPUT" + RULE_SPEC;
        String addOutput = iptables + " -A OUTPUT" + RULE_SPEC;
        new SuTask().execute(deleteInput, deleteOutput, addInput, addOutput);
    }

    public static void deleteRules(Context context) {
        String iptables = InstallUtils.getIptables().getFile(context).getAbsolutePath();
        String input = iptables + " -D INPUT" + RULE_SPEC;
        String output = iptables + " -D OUTPUT" + RULE_SPEC;
        new SuTask().execute(input, output);
    }
    
    

}
