package de.pure.logger;

import android.util.Log;
import de.pure.logger.controller.FileObserveTask;

import java.util.ArrayList;
import java.util.Timer;

public class Global {

    public static final String EXPORT_CSVFILE = "alllogentries.csv";
    public static final int INTERVAL_TIME = 10000;
    public static Timer repeater;
    public static boolean isSendingBeingRepeatedRightNow;
    public static StringBuffer report;
    /*
        * Database name
        */
    public static String DB_SCHEMA_NAME = "de_pure_logger_db";


    public static final String TAG = "PrivacyAccessLogger";


    public static final String SYSTEMTAP_FILE = "testfile.txt";


    public static final int DB_VERSION = 1;


    public static final String EXTRA_PRIVACY_ISALIVE_MESSENGER = "Messenger";


    public static final String FAKE_LOG_PACKAGENAME = "FAKE";

    public static final String FILEWATCH_LOG_PACKAGENAME = "FILEWATCH";

    public static final String NETWORK_LOG_PACKAGENAME = "NETWORK";


    public static final String ACTION_XPRIVACY_LOG_NOTIFICATION =
            "de.pure.logger.LOG_NOTIFICATION";

    public static final Integer INVALID_UID = -1;

    public static ArrayList<FileObserveTask> fileObserveTasks;

    /**
     * LogCat stuff
     */

    // set those accordingly
    private static final boolean shouldLogCatDebug = true;
    private static final boolean shouldLogCatError = true;
    private static final boolean shouldLogCatWarn = true;

    public static void logcatDebug(String toLog) {
        if (shouldLogCatDebug) {
            Log.d(TAG, toLog);
        }
    }

    public static void logcatError(String toLog) {
        if (shouldLogCatError) {
            Log.e(TAG, toLog);
        }
    }

    public static void logcatWarn(String toLog) {
        if (shouldLogCatWarn) {
            Log.w(TAG, toLog);
        }
    }
}
