package de.pure.logger.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import de.pure.logger.Global;
import de.pure.logger.entity.XPrivacyLogEntry;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

class DbTableXPrivacyLogEntry {


    static boolean writeXPrivacyLogEntry(SQLiteDatabase database, XPrivacyLogEntry xPrivacyLogEntry) {
        boolean success = false;
        if (database != null) {
            database.beginTransaction();
            try {
                ContentValues values = DbTableXPrivacyLogEntry.xPrivacyLogEntry2Values(xPrivacyLogEntry);
                success = database.insert(
                        DbTableXPrivacyLogEntry.TBL_XPRIVACYLOGENTRY_TABLE_NAME, null, values) > 0;
                database.setTransactionSuccessful();
            } catch (SQLiteException e) {
                success = false;
                Global.logcatError("SQLiteException: " + e.getMessage());
            } finally {
                database.endTransaction();
            }

        } else {
            Global.logcatError("database is null!");
        }
        return success;
    }

    static boolean writeXPrivacyLogEntries(SQLiteDatabase database, Collection<XPrivacyLogEntry> xPrivacyLogEntryList) {
        boolean success = false;
        if (database != null) {
            database.beginTransaction();
            try {
                for (XPrivacyLogEntry entry : xPrivacyLogEntryList) {
                    ContentValues values = DbTableXPrivacyLogEntry.xPrivacyLogEntry2Values(entry);
                    database.insert(
                            DbTableXPrivacyLogEntry.TBL_XPRIVACYLOGENTRY_TABLE_NAME, null, values);
                }
                database.setTransactionSuccessful();
                success = true;
            } catch (SQLiteException e) {
                success = false;
                Global.logcatError("SQLiteException: " + e.getMessage());
            } finally {
                database.endTransaction();
            }
        } else {
            Global.logcatError("database is null!");
        }
        return success;
    }

    static ArrayList<XPrivacyLogEntry> getAllXPrivacyLogEntries(SQLiteDatabase database, int limit) {

        ArrayList<XPrivacyLogEntry> result = new ArrayList<XPrivacyLogEntry>();
        if (database != null) {
            Cursor c = null;
            try {
                String qry =
                        DbUtils.STMT_SELECT
                                + DbUtils.EVERYTHING
                                + DbUtils.STMT_FROM
                                + DbTableXPrivacyLogEntry.TBL_XPRIVACYLOGENTRY_TABLE_NAME
                                + DbUtils.ORDERBY
                                + DbTableXPrivacyLogEntry.TBL_XPRIVACYLOGENTRY_COL_CREATIONDATE
                                + DbUtils.DESC;
                if (limit > 0) {
                    qry += DbUtils.LIMIT + limit;
                }

                c = database.rawQuery(qry, null);
                result = DbTableXPrivacyLogEntry.cursor2LogEntries(c);
            } catch (SQLiteException e) {
                Global.logcatError("SQLiteException: " + e.getMessage());
            } finally {
                if (c != null) {
                    c.close();
                }
            }
            if (result == null) {
                return new ArrayList<XPrivacyLogEntry>();
            }
        } else {
            Global.logcatError("database is null!");
        }
        return result;
    }

    static ArrayList<XPrivacyLogEntry> getAllXPrivacyLogEntriesNotOlderThan(
            SQLiteDatabase database, int maxAgeInSeconds) {
        ArrayList<XPrivacyLogEntry> result = new ArrayList<XPrivacyLogEntry>();
        if (database != null) {
            Cursor c = null;
            try {
                if (maxAgeInSeconds > 0) {
                    Date now = new Date();
                    long minimumTime = now.getTime() - (maxAgeInSeconds * 1000);
                    final String qry = DbUtils.STMT_SELECT
                            + DbUtils.EVERYTHING
                            + DbUtils.STMT_FROM
                            + DbTableXPrivacyLogEntry.TBL_XPRIVACYLOGENTRY_TABLE_NAME
                            + DbUtils.STMT_WHERE
                            + DbTableXPrivacyLogEntry.TBL_XPRIVACYLOGENTRY_COL_CREATIONDATE
                            + DbUtils.ISGREATEROREQUAL
                            + minimumTime
                            + DbUtils.ORDERBY
                            + DbTableXPrivacyLogEntry.TBL_XPRIVACYLOGENTRY_COL_CREATIONDATE
                            + DbUtils.DESC;

                    c = database.rawQuery(qry, null);

                    result = DbTableXPrivacyLogEntry.cursor2LogEntries(c);
                }
            } catch (SQLiteException e) {
                Global.logcatError("SQLiteException: " + e.getMessage());
            } finally {
                if (c != null) {
                    c.close();
                }
            }
            if (result == null) {
                return new ArrayList<XPrivacyLogEntry>();
            }
        } else {
            Global.logcatError("database is null!");
        }
        return result;
    }


    static final String TBL_XPRIVACYLOGENTRY_TABLE_NAME = "xprivacy_logs";

    private static final String TBL_XPRIVACYLOGENTRY_COL_DB_ID = DbUtils.DATABASE_TBL_COL_ID;
    private static final String TBL_XPRIVACYLOGENTRY_COL_CREATIONDATE = "creationdate";
    private static final String TBL_XPRIVACYLOGENTRY_COL_PACKAGENAME = "packagename";
    private static final String TBL_XPRIVACYLOGENTRY_COL_UID = "uid";
    private static final String TBL_XPRIVACYLOGENTRY_COL_CONTENT = "content";


    private static final String TBL_XPRIVACYLOGENTRY_COL_DB_ID_TYPE = DbUtils.DATATYPE_ID;
    private static final String TBL_XPRIVACYLOGENTRY_COL_CREATIONDATE_TYPE = DbUtils.DATATYPE_TEXT;
    private static final String TBL_XPRIVACYLOGENTRY_COL_PACKAGENAME_TYPE = DbUtils.DATATYPE_TEXT;
    private static final String TBL_XPRIVACYLOGENTRY_COL_UID_TYPE = DbUtils.DATATYPE_INTEGER;
    private static final String TBL_XPRIVACYLOGENTRY_COL_CONTENT_TYPE = DbUtils.DATATYPE_TEXT;

    private static final String TBL_XPRIVACYLOGENTRY_COLUMNS = ""

            + "  " + TBL_XPRIVACYLOGENTRY_COL_DB_ID + " " + TBL_XPRIVACYLOGENTRY_COL_DB_ID_TYPE
            + ", " + TBL_XPRIVACYLOGENTRY_COL_CREATIONDATE + " " + TBL_XPRIVACYLOGENTRY_COL_CREATIONDATE_TYPE
            + ", " + TBL_XPRIVACYLOGENTRY_COL_PACKAGENAME + " " + TBL_XPRIVACYLOGENTRY_COL_PACKAGENAME_TYPE

            + ", " + TBL_XPRIVACYLOGENTRY_COL_UID + " " + TBL_XPRIVACYLOGENTRY_COL_UID_TYPE
            + ", " + TBL_XPRIVACYLOGENTRY_COL_CONTENT + " " + TBL_XPRIVACYLOGENTRY_COL_CONTENT_TYPE;

    static final String TBL_XPRIVACYLOGENTRY_CREATETABLE = DbUtils.STMT_CREATETABLE
            + TBL_XPRIVACYLOGENTRY_TABLE_NAME + DbUtils.OPENBRACKET
            + TBL_XPRIVACYLOGENTRY_COLUMNS + DbUtils.CLOSEBRACKET;
    static final String TBL_XPRIVACYLOGENTRY_DELETEALLFROMTABLE = DbUtils.STMT_DELETEFROMTABLE
            + TBL_XPRIVACYLOGENTRY_TABLE_NAME;
    static final String TBL_XPRIVACYLOGENTRY_DROPTABLE = DbUtils.STMT_DROPTABLEIFEXISTS
            + TBL_XPRIVACYLOGENTRY_TABLE_NAME;


    private static ArrayList<XPrivacyLogEntry> cursor2LogEntries(Cursor c) {

        ArrayList<XPrivacyLogEntry> result = new ArrayList<XPrivacyLogEntry>();

        if (c != null) {
            while (c.moveToNext()) {
                try {
                    int index;

                    XPrivacyLogEntry xPrivacyLogEntry = new XPrivacyLogEntry();

                    index = c.getColumnIndexOrThrow(TBL_XPRIVACYLOGENTRY_COL_CREATIONDATE);
                    // parse String to long to Date
                    xPrivacyLogEntry.creationDate = new Date(Long.parseLong(c.getString(index)));

                    index = c.getColumnIndexOrThrow(TBL_XPRIVACYLOGENTRY_COL_PACKAGENAME);
                    xPrivacyLogEntry.packageName = c.getString(index);

                    index = c.getColumnIndexOrThrow(TBL_XPRIVACYLOGENTRY_COL_UID);
                    xPrivacyLogEntry.uid = c.getInt(index);

                    index = c.getColumnIndexOrThrow(TBL_XPRIVACYLOGENTRY_COL_CONTENT);
                    xPrivacyLogEntry.content = c.getString(index);

                    result.add(xPrivacyLogEntry);

                } catch (IllegalArgumentException e) {
                    Global.logcatError(e.getLocalizedMessage());
                }
            }
        }
        return result;
    }


    private static XPrivacyLogEntry cursor2XPrivacyLogEntry(Cursor c) {

        XPrivacyLogEntry xPrivacyLogEntry = new XPrivacyLogEntry();

        if (c != null) {
            if (c.moveToFirst()) {
                try {
                    int index;

                    index = c.getColumnIndexOrThrow(TBL_XPRIVACYLOGENTRY_COL_CREATIONDATE);
                    // parse String to long to Date
                    xPrivacyLogEntry.creationDate = new Date(Long.parseLong(c.getString(index)));

                    index = c.getColumnIndexOrThrow(TBL_XPRIVACYLOGENTRY_COL_PACKAGENAME);
                    xPrivacyLogEntry.packageName = c.getString(index);

                    index = c.getColumnIndexOrThrow(TBL_XPRIVACYLOGENTRY_COL_UID);
                    xPrivacyLogEntry.uid = c.getInt(index);

                    index = c.getColumnIndexOrThrow(TBL_XPRIVACYLOGENTRY_COL_CONTENT);
                    xPrivacyLogEntry.content = c.getString(index);

                    return xPrivacyLogEntry;

                } catch (IllegalArgumentException e) {
                    Global.logcatError(e.getLocalizedMessage());
                }
            } else {
                Global.logcatWarn("Did not find any results!");
            }

        }
        return null;
    }

    private static ContentValues xPrivacyLogEntry2Values(XPrivacyLogEntry xPrivacyLogEntry) {
        ContentValues values = new ContentValues();

        if (xPrivacyLogEntry != null) {

            Calendar c = Calendar.getInstance();
            if (xPrivacyLogEntry.creationDate != null) {
                c.setTime(xPrivacyLogEntry.creationDate);
            }
            values.put(TBL_XPRIVACYLOGENTRY_COL_CREATIONDATE, "" + c.getTimeInMillis());

            if (xPrivacyLogEntry.packageName != null) {
                values.put(TBL_XPRIVACYLOGENTRY_COL_PACKAGENAME, xPrivacyLogEntry.packageName);
            } else {
                values.put(TBL_XPRIVACYLOGENTRY_COL_PACKAGENAME, "");
            }
            if (xPrivacyLogEntry.content != null) {
                values.put(TBL_XPRIVACYLOGENTRY_COL_CONTENT, xPrivacyLogEntry.content);
            } else {
                values.put(TBL_XPRIVACYLOGENTRY_COL_CONTENT, "");
            }

            values.put(TBL_XPRIVACYLOGENTRY_COL_UID, xPrivacyLogEntry.uid);
        }

        return values;
    }

}