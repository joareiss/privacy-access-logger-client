package de.pure.logger.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import de.pure.logger.Global;
import de.pure.logger.entity.AppInfo;
import de.pure.logger.entity.XPrivacyLogEntry;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Not using SQLiteOpenHelper directly, rather
 * using Singleton and only one connection at all times
 * <p/>
 * This helps getting rid of concurrency issues when multiple threads want to write/read
 * from the database at the same time. The single connection enables using the already
 * existing concurrency handling of the SQLite framework.
 * <p/>
 * https://github.com/dmytrodanylyk/dmytrodanylyk/blob/gh-pages/articles/Concurrent%20Database%20Access.md
 */
public class DbCom {

    private AtomicInteger mOpenCounter = new AtomicInteger();

    private static DbCom instance;
    private static MySQLiteOpenHelper dbCom;
    private SQLiteDatabase mDatabase;


    public static synchronized DbCom getInstance(Context context) {
        if (instance == null) {
            //  throw new IllegalStateException(DbCom.class.getSimpleName() +
            //         " is not initialized, call initializeInstance(..) method first.");
            initializeInstance(new MySQLiteOpenHelper(context));
        }
        return instance;
    }


    public static SQLiteDatabase getDatabase(Context context) {
        return getInstance(context).openDatabase();
    }

    public SQLiteDatabase openDatabase() {
        if (mOpenCounter.incrementAndGet() == 1) {
            // Opening new database
            mDatabase = dbCom.getWritableDatabase();
        }
        return mDatabase;
    }

    public void closeDatabase() {
        if (mOpenCounter.decrementAndGet() == 0) {
            // Closing database
            mDatabase.close();
        }
    }


    public static void clearLogs(SQLiteDatabase database) {
        if (database != null) {
            DbUtils.execSql(database, DbTableXPrivacyLogEntry.TBL_XPRIVACYLOGENTRY_DELETEALLFROMTABLE);
        } else {
            Global.logcatError("database is null!");
        }
    }


    public static void clearAllTables(SQLiteDatabase database) {
        if (database != null) {
            DbUtils.execSql(database, DbTableXPrivacyLogEntry.TBL_XPRIVACYLOGENTRY_DELETEALLFROMTABLE);
            DbUtils.execSql(database, DbTableWhatToLog.TBL_WHATTOLOG_DELETEALLFROMTABLE);
        } else {
            Global.logcatError("database is null!");
        }
    }

    private static synchronized void initializeInstance(MySQLiteOpenHelper dc) {
        if (instance == null) {
            instance = new DbCom();
            dbCom = dc;
        }
    }

    //
    // METHODS
    //


    public static boolean writeXPrivacyLogEntry(SQLiteDatabase database, XPrivacyLogEntry logEntry) {
        return DbTableXPrivacyLogEntry.writeXPrivacyLogEntry(database, logEntry);
    }

    public static boolean writeXPrivacyLogEntries(SQLiteDatabase database,
                                                  Collection<XPrivacyLogEntry> logEntries) {
        return DbTableXPrivacyLogEntry.writeXPrivacyLogEntries(database, logEntries);
    }

    public static ArrayList<XPrivacyLogEntry> getAllXPrivacyLogEntries(SQLiteDatabase database) {
        return DbTableXPrivacyLogEntry.getAllXPrivacyLogEntries(database, -1);
    }

    public static ArrayList<XPrivacyLogEntry> getAllXPrivacyLogEntries(SQLiteDatabase database, int limit) {
        return DbTableXPrivacyLogEntry.getAllXPrivacyLogEntries(database, limit);
    }

    public static ArrayList<XPrivacyLogEntry> getAllXPrivacyLogEntriesNotOlderThan(SQLiteDatabase database, int maxAgeInSeconds) {
        return DbTableXPrivacyLogEntry.getAllXPrivacyLogEntriesNotOlderThan(database, maxAgeInSeconds);
    }

//
//    public static boolean writeLogEntry(SQLiteDatabase database, LogEntry logEntry) {
//        return DbTableLogEntry.writeLogEntry(database, logEntry);
//    }
//
//    public static boolean writeLogEntries(SQLiteDatabase database,
//                                          List<LogEntry> logEntryList) {
//        return DbTableLogEntry.writeLogEntries(database, logEntryList);
//    }
//
//    public static ArrayList<LogEntry> getAllLogEntries(SQLiteDatabase database) {
//        return DbTableLogEntry.getAllLogEntries(database, -1);
//    }
//
//    public static ArrayList<LogEntry> getAllLogEntries(SQLiteDatabase database, int limit) {
//        return DbTableLogEntry.getAllLogEntries(database, limit);
//    }
//
//    public static ArrayList<LogEntry> getAllLogEntriesNotOlderThan(SQLiteDatabase database, int maxAgeInSeconds) {
//        return DbTableLogEntry.getAllLogEntriesNotOlderThan(database, maxAgeInSeconds);
//    }


    public static boolean deleteAppInfo(SQLiteDatabase database, String packageName) {
        return DbTableWhatToLog.deleteAppInfo(database, packageName);
    }

    public static boolean writeAppInfo(SQLiteDatabase database, AppInfo appInfo) {
        return DbTableWhatToLog.writeAppInfo(database, appInfo);
    }

    public static boolean writeAppInfos(SQLiteDatabase database,
                                        List<AppInfo> appInfoList) {
        return DbTableWhatToLog.writeAppInfos(database, appInfoList);
    }

    public static ArrayList<AppInfo> getAllAppInfos(SQLiteDatabase database) {
        return DbTableWhatToLog.getAllAppInfos(database);
    }

}