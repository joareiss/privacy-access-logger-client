package de.pure.logger.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import de.pure.logger.Global;

/**
 * Database Communicator
 */
class MySQLiteOpenHelper extends SQLiteOpenHelper {

    public MySQLiteOpenHelper(Context context) {
        super(context, Global.DB_SCHEMA_NAME, null, Global.DB_VERSION);
        Global.logcatWarn("MySQLiteOpenHelper Constructor");
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Global.logcatWarn("CREATING DATABASE");
        DbUtils.execSql(sqLiteDatabase, DbTableXPrivacyLogEntry.TBL_XPRIVACYLOGENTRY_CREATETABLE);
        DbUtils.execSql(sqLiteDatabase, DbTableWhatToLog.TBL_WHATTOLOG_CREATETABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        Global.logcatWarn(String.format("Upgrading database from version " +
                "%d to %d, which will destroy all old data",
                oldVersion, newVersion));
        DbUtils.execSql(sqLiteDatabase, DbTableXPrivacyLogEntry.TBL_XPRIVACYLOGENTRY_DROPTABLE);
        DbUtils.execSql(sqLiteDatabase, DbTableWhatToLog.TBL_WHATTOLOG_DROPTABLE);
        onCreate(sqLiteDatabase);
    }


}
