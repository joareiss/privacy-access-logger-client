package de.pure.logger.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import de.pure.logger.Global;
import de.pure.logger.entity.AppInfo;

import java.util.ArrayList;
import java.util.List;

class DbTableWhatToLog {


    public static boolean deleteAppInfo(SQLiteDatabase database, String packageName) {
        boolean success = false;
        if (database != null) {
            database.beginTransaction();
            try {
                success = database.delete(
                        TBL_WHATTOLOG_TABLE_NAME,
                        TBL_WHATTOLOG_COL_PACKAGENAME + "=?",
                        new String[]{packageName}) > 0;
                database.setTransactionSuccessful();
            } catch (SQLiteException e) {
                success = false;
                Global.logcatError("SQLiteException: " + e.getMessage());
            } finally {
                database.endTransaction();
            }

        } else {
            Global.logcatError("database is null!");
        }
        return success;
    }

    static boolean writeAppInfo(SQLiteDatabase database, AppInfo appInfo) {
        boolean success = false;
        if (database != null) {
            database.beginTransaction();
            try {
                ContentValues values = appInfo2Values(appInfo);
                success = database.insert(
                        TBL_WHATTOLOG_TABLE_NAME, null, values) > 0;
                database.setTransactionSuccessful();
            } catch (SQLiteException e) {
                success = false;
                Global.logcatError("SQLiteException: " + e.getMessage());
            } finally {
                database.endTransaction();
            }

        } else {
            Global.logcatError("database is null!");
        }
        return success;
    }

    static boolean writeAppInfos(SQLiteDatabase database,
                                 List<AppInfo> appInfoList) {
        boolean success = false;
        if (database != null) {
            database.beginTransaction();
            try {
                for (AppInfo appinfo : appInfoList) {
                    ContentValues values = appInfo2Values(appinfo);
                    database.insert(
                            TBL_WHATTOLOG_TABLE_NAME, null, values);
                }
                database.setTransactionSuccessful();
                success = true;
            } catch (SQLiteException e) {
                success = false;
                Global.logcatError("SQLiteException: "
                        + e.getMessage());
            } finally {
                database.endTransaction();
            }
        } else {
            Global.logcatError("database is null!");
        }
        return success;
    }


    /**
     * @param database
     * @param packageName will be used in WHERE clause if set
     * @return an AppInfo or null
     */
    static AppInfo getAppInfo(SQLiteDatabase database,
                              String packageName) {
        AppInfo result = null;
        if (database != null) {
            Cursor c = null;
            try {
                int limit = 100;
                String qry = "";

                qry += DbUtils.STMT_SELECT + DbUtils.EVERYTHING
                        + DbUtils.STMT_FROM + TBL_WHATTOLOG_TABLE_NAME;

                qry += DbUtils.STMT_WHERE
                        + TBL_WHATTOLOG_COL_PACKAGENAME
                        + DbUtils.EQUALS + DbUtils.QT
                        + packageName + DbUtils.QT;

                c = database.rawQuery(qry, null); // TODO use database.query rather than rawQuery?
                result = cursor2AppInfo(c);
            } catch (SQLiteException e) {
                Global.logcatError("SQLiteException: " + e.getMessage());
            } finally {
                if (c != null) {
                    c.close();
                }
            }
        } else {
            Global.logcatError("database is null!");
        }
        return result;
    }

    static ArrayList<AppInfo> getAllAppInfos(SQLiteDatabase database) {
        ArrayList<AppInfo> result = new ArrayList<AppInfo>();
        if (database != null) {
            Cursor c = null;
            try {
                int limit = 100;
                c = database.rawQuery(
                        "SELECT * FROM " + TBL_WHATTOLOG_TABLE_NAME, null);
                result = cursor2AppInfos(c);
            } catch (SQLiteException e) {
                Global.logcatError("SQLiteException: " + e.getMessage());
            } finally {
                if (c != null) {
                    c.close();
                }
            }
            if (result == null) {
                return new ArrayList<AppInfo>();
            }
        } else {
            Global.logcatError("database is null!");
        }
        return result;
    }


    static final String TBL_WHATTOLOG_TABLE_NAME = "whattolog";

    private static final String TBL_WHATTOLOG_COL_DB_ID = DbUtils.DATABASE_TBL_COL_ID;
    private static final String TBL_WHATTOLOG_COL_PACKAGENAME = "packagename";
    private static final String TBL_WHATTOLOG_COL_APPNAME = "appname";


    private static final String TBL_WHATTOLOG_COL_DB_ID_TYPE = DbUtils.DATATYPE_ID;
    private static final String TBL_WHATTOLOG_COL_PACKAGENAME_TYPE = DbUtils.DATATYPE_TEXT;
    private static final String TBL_WHATTOLOG_COL_APPNAME_TYPE = DbUtils.DATATYPE_TEXT;

    private static final String TBL_WHATTOLOG_COLUMNS = ""

            + "  " + TBL_WHATTOLOG_COL_DB_ID + " " + TBL_WHATTOLOG_COL_DB_ID_TYPE
            + ", " + TBL_WHATTOLOG_COL_PACKAGENAME + " " + TBL_WHATTOLOG_COL_PACKAGENAME_TYPE
            + ", " + TBL_WHATTOLOG_COL_APPNAME + " " + TBL_WHATTOLOG_COL_APPNAME_TYPE;

    static final String TBL_WHATTOLOG_CREATETABLE = DbUtils.STMT_CREATETABLE
            + TBL_WHATTOLOG_TABLE_NAME + DbUtils.OPENBRACKET
            + TBL_WHATTOLOG_COLUMNS + DbUtils.CLOSEBRACKET;
    static final String TBL_WHATTOLOG_DELETEALLFROMTABLE = DbUtils.STMT_DELETEFROMTABLE
            + TBL_WHATTOLOG_TABLE_NAME;
    static final String TBL_WHATTOLOG_DROPTABLE = DbUtils.STMT_DROPTABLEIFEXISTS
            + TBL_WHATTOLOG_TABLE_NAME;


    private static ArrayList<AppInfo> cursor2AppInfos(Cursor c) {

        ArrayList<AppInfo> result = new ArrayList<AppInfo>();

        if (c != null) {
            while (c.moveToNext()) {
                try {
                    int index;

                    AppInfo appInfo = new AppInfo();

                    index = c.getColumnIndexOrThrow(TBL_WHATTOLOG_COL_PACKAGENAME);
                    appInfo.packageName = c.getString(index);

                    index = c.getColumnIndexOrThrow(TBL_WHATTOLOG_COL_APPNAME);
                    appInfo.appName = c.getString(index);

                    result.add(appInfo);

                } catch (IllegalArgumentException e) {
                    Global.logcatError(e.getLocalizedMessage());
                }
            }
        }
        return result;
    }


    private static AppInfo cursor2AppInfo(Cursor c) {
        if (c != null) {
            if (c.moveToFirst()) {
                try {
                    int index;

                    AppInfo appInfo = new AppInfo();

                    index = c.getColumnIndexOrThrow(TBL_WHATTOLOG_COL_PACKAGENAME);
                    appInfo.packageName = c.getString(index);

                    index = c.getColumnIndexOrThrow(TBL_WHATTOLOG_COL_APPNAME);
                    appInfo.appName = c.getString(index);

                    return appInfo;

                } catch (IllegalArgumentException e) {
                    Global.logcatError(e.getLocalizedMessage());
                }
            } else {
                Global.logcatError("Did not find any results!");
            }

        }
        return null;
    }

    private static ContentValues appInfo2Values(AppInfo appInfo) {
        ContentValues values = new ContentValues();

        if (appInfo != null) {

            if (appInfo.packageName != null) {
                values.put(TBL_WHATTOLOG_COL_PACKAGENAME, appInfo.packageName);
            } else {
                values.put(TBL_WHATTOLOG_COL_PACKAGENAME, "");
            }

            if (appInfo.appName != null) {
                values.put(TBL_WHATTOLOG_COL_APPNAME, appInfo.appName);
            } else {
                values.put(TBL_WHATTOLOG_COL_APPNAME, "");
            }

        }

        return values;
    }

}