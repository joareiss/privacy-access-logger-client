package de.pure.logger.data;

import android.database.sqlite.SQLiteDatabase;
import de.pure.logger.Global;

class DbUtils {


    public static void execSql(SQLiteDatabase database, String raw) {
        if (database != null) {
            database.beginTransaction();
            try {
                database.execSQL(raw);
                database.setTransactionSuccessful();
            } catch (Exception e) {
                Global.logcatError(e.getMessage());
            } finally {
                database.endTransaction();
            }
        } else {
            Global.logcatError("database is null!");
        }
    }


    public static final String QT = "'";
    public static final String OPENBRACKET = " ( ";
    public static final String CLOSEBRACKET = " ) ";
    public static final String AND = " AND ";
    public static final String EQUALS = " = ";
    public static final String ISGREATEROREQUAL = " >= ";
    public static final String ISLESSOREQUAL = " <= ";
    public static final String DESC = " DESC ";
    public static final String ASC = " ASC ";
    public static final String EVERYTHING = " * ";
    public static final String ORDERBY = " ORDER BY ";
    public static final String LIMIT = " LIMIT ";


    /*
     * Database data types
     */
    public static final String DATATYPE_ID = " INTEGER PRIMARY KEY AUTOINCREMENT ";
    public static final String DATATYPE_TEXT = " TEXT NOT NULL ";
    public static final String DATATYPE_INTEGER = " INTEGER ";


    /*
     * Statements
     */
    public static final String STMT_FROM = " FROM ";
    public static final String STMT_SELECT = " SELECT ";
    public static final String STMT_WHERE = " WHERE ";
    public static final String STMT_DELETEFROMTABLE = " DELETE " + STMT_FROM;
    public static final String STMT_CREATETABLE = " CREATE TABLE IF NOT EXISTS ";
    public static final String STMT_DROPTABLEIFEXISTS = " DROP TABLE IF EXISTS ";


    /**
     * Unique row id
     */
    public static final String DATABASE_TBL_COL_ID = "__id";
}
