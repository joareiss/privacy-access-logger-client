Privacy Access Logger is an application created by Jonas Pusch and
Joachim Reiß during a project in 2013/2014 at the University of Marburg.

It relies on the XPrivacy Framework that can
be found at
https://github.com/JoachimR/XPrivacy/ .
With this app it is possible to receive the broadcast events that are
sent by XPrivacy and log those events accordingly to a SQLite database.
The logs can be exported to a .csv file
on the external storage.

Additionally, since XPrivacy does not
catch native file access events done by Android Apps via the NDK,
a FileWatcher functionality is provided in this application.
Such a FileWatcher can create
log events, given the respective file
to watch has been accessed in any way.

While XPrivacy is able to catch network calls from Android,
it also has problems when Apps send network packets via the NDK.
However, to provide logging for the whole traffic, we implemented
an additional network logging mechanism based on the linux tools 
iptables & nflog which intercept network packets on a lower layer.
